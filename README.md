## CI-COMMON

Used to aggregate common CI templates using [include: remote](https://docs.gitlab.com/ee/ci/yaml/#includeremote)

[Demo project](https://gitlab.com/colisweb-idl/colisweb/pocs/ci-demo) using this template.

### Elements of the pipeline should use the following conventions:

```yaml
stage service branch
compile ourService master:
  ...
```

### To avoid repetitions, extends templates with more specific ones

```yaml
.specific-cache-job:
  extends: .sbt-pull-push-cache
  variables:
    STASH: "application-specific-cache"


# Use specific template in pipeline job
compile ourService master:
  extends: .specific-cache-job
  ...
```

## Stages

The list and order of all stages is defined [here](./.gitlab-ci/templates/colisweb.yml)

- ***compile***: Compilation (i.e: for our scala services) and format check of the codebase
- ***test***: Unit testing, unused dependencies or behaviour tests
- ***run***: Specific stage for Scheduled jobs like data export or analysis
- ***build***: Creation of docker images
- ***publish***: Publishing of docker images on docker hub or AWS ECR
- ***promote-staging***: Promoting the docker images for a release on staging
- ***migrate-staging***: Database migration on the staging environment like postgresql, mysql or BigQuery
- ***deploy-staging***: Deployment to staging environment as well as a sentry release
- ***check***: Testing deployments using E2E tests (use to block production deployment)
- ***version***: Semantic-versioning for releases and tags management + update recette from master
- ***promote***: Promoting the docker images for a tagged release or a environment-related release
- ***migrate***: Database migration like postgresql, mysql or BigQuery
- ***deploy***: Deployment to production or testing environment as well as sentry release deployment
- ***benchmark***: Long running benchmarks for performance analysis
- ***publish-benchmark***: Publishing benchmarks results in a gitlab environment and pages

---

***test*** can be skipped by adding ***[SAT]*** (Skip automated test) in the commit message

***version*** can be skipped by adding ***[NMT]*** (Need Manual Testing) in the commit message. **It will prevent all automated release if the flag is present in the history since the last release**. This prevents unwanted releases to happen (i.e: scala-steward automated updates)

---
## Pipelines

There are different possible pipelines, which uses a subset of all possible stages.
The pipelines that the rules and related jobs have been setup for are:
- ***branch pipelines***: these pipelines will ensure the change are sane. Common jobs are compilation and tests
- ***testing pipelines***: this pipeline to deploy the code to the testing environment after asserting its correctness like for branch pipelines.
- ***recette pipelines***: this pipeline will deploy the code to the recette environment after asserting its correctness like for branch pipelines (same behavior as testing).
- ***master pipelines***: this pipeline will create a version (tag) for the code after asserting its correctness through the ***check*** stage
- ***tag pipelines***: triggered when a tag is created, they will usually deploy to the production environment or publish a library release
- ***scheduled pipelines***: Use for scheduled job, like weekly migration or analysis

```plantuml
@startuml
title branch
:compile;
:test;
:benchmarks;
:publish-benchmarks;
@enduml
```

```plantuml
@startuml
title testing
:compile;
:test;
:build;
:publish;
:promote;
:migrate;
:deploy;
@enduml
```

```plantuml
@startuml
title master
:compile;
:test;
:build;
:publish;
:promote-staging;
:migrate-staging;
:deploy-staging;
:check;
:version;
@enduml
```

```plantuml
@startuml
title tag
:promote;
:migrate;
:deploy;
:benchmarks;
:publish-benchmarks;
@enduml
```

```plantuml
@startuml
title scheduled
:compile*;
:test*;
:run;
@enduml
```

Stages in the Scheduled pipeline noted with * are optional. An [extra configuration](./.gitlab-ci/rules/rules.yml) is needed to activate them.
 

### [Semantic versioning](https://www.conventionalcommits.org/en/v1.0.0/#summary)

***version*** jobs are using semantic versioning in order to create tags. These tags then trigger pipelines for production deployment or library releases

# Upgrade to version v17

## How it works
Before, all dev-tools functions were appended to `before_script` within each job. Now a specific secure file will be created for each version of the new CI.
These files will be downloaded securely and sourced at runtime during the `before_script` test within each job.


## How to upgrade

Beware `before_script` is imported and executed before all scripts. This keyword cannot be templated and so will be imported automatically.  
However `before_script` needs an available `CI_JOB_TOKEN`.  
This variable depends on the privilege of the one who start the job. No particular setup should be needed.  
Also if you had jobs referencing `export_functions`, they now need to reference `import_functions`. This keyword also need the variable `GITLAB_FUNCTION_TOKEN` to be available
If you need it for Open-source project, make it available by adding it only for protected pipelines (tag and master). Make sure to select the masked property if available. 

**_NOTE :_**  **pipeline where the token does not have the proper rights will run but always fail**

This is the current `before_script`
```yaml
before_script:
  - !reference [.before_template, reveal_secrets]
  - !reference [.before_template, export_variables]
  - !reference [.before_template, import_functions]
```

